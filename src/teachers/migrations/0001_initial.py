from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Teacher",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("first_name", models.CharField(blank=True, max_length=120, null=True)),
                ("last_name", models.CharField(blank=True, max_length=120, null=True)),
                ("email", models.EmailField(max_length=120)),
                (
                    "phone_number",
                    models.CharField(blank=True, max_length=120, null=True),
                ),
                ("birth_date", models.DateField(blank=True, null=True)),
                (
                    "rating_teacher",
                    models.DecimalField(decimal_places=2, default=0, max_digits=3),
                ),
            ],
        ),
    ]
