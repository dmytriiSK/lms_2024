from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("teachers", "0001_initial"),
    ]

    operations = [
        migrations.RenameModel(
            old_name="Teacher",
            new_name="Teachers",
        ),
    ]
