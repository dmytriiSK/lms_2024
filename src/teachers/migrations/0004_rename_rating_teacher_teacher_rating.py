# Generated by Django 4.2.11 on 2024-04-25 20:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("teachers", "0003_rename_teachers_teacher"),
    ]

    operations = [
        migrations.RenameField(
            model_name="teacher",
            old_name="rating_teacher",
            new_name="rating",
        ),
    ]
