from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("teachers", "0002_rename_teacher_teachers"),
    ]

    operations = [
        migrations.RenameModel(
            old_name="Teachers",
            new_name="Teacher",
        ),
    ]
