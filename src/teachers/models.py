from decimal import Decimal

from django.db import models
from faker import Faker


class Teacher(models.Model):
    first_name = models.CharField(max_length=120, blank=True, null=True)
    last_name = models.CharField(max_length=120, blank=True, null=True)
    email = models.EmailField(max_length=120)
    phone_number = models.CharField(max_length=120, blank=True, null=True)
    birth_date = models.DateField(blank=True, null=True)
    rating = models.DecimalField(decimal_places=2, max_digits=3, default=0)

    @classmethod
    def generate_teachers(cls, count: int) -> None:
        faker = Faker()
        for i in range(count):
            teacher = Teacher(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                phone_number=faker.phone_number(),
                birth_date=faker.date_of_birth(minimum_age=16, maximum_age=60),
                rating=Decimal(faker.pydecimal(min_value=0, max_value=5, right_digits=2)),
            )
            teacher.save()

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.phone_number} {self.rating} ({self.id})"
