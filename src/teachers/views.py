from django.db.models import Q
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from webargs import fields
from webargs.djangoparser import use_args

from teachers.forms import TeacherForm
from teachers.models import Teacher


@use_args(
    {
        "first_name": fields.Str(required=False),
        "last_name": fields.Str(required=False),
        "search": fields.Str(required=False),
    },
    location="query",
)
def get_teachers(request: HttpRequest, params) -> HttpResponse:
    teachers = Teacher.objects.all()
    search_fields = ["first_name", "last_name", "email", "phone_number"]
    for param_name, param_value in params.items():
        if param_name == "search":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__contains": param_value})
                teachers = teachers.filter(or_filter)
        else:
            teachers = teachers.filter(**{param_name: param_value})
    return render(request, template_name="list_teacher.html", context={"teachers": teachers})


@csrf_exempt
def create_teacher(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = TeacherForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("teachers:get_teachers"))
    elif request.method == "GET":
        form = TeacherForm()

    return render(request, template_name="create_teacher.html", context={"form": form})


@csrf_exempt
def update_teacher(request: HttpRequest, pk: int) -> HttpResponse:
    teacher = get_object_or_404(Teacher.objects.all(), pk=pk)

    if request.method == "POST":
        form = TeacherForm(request.POST, instance=teacher)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("teachers:get_teachers"))
    elif request.method == "GET":
        form = TeacherForm(instance=teacher)

    return render(request, template_name="update_teacher.html", context={"form": form})


@csrf_exempt
def delete_teacher(request: HttpRequest, pk: int) -> HttpResponse:
    teacher = get_object_or_404(Teacher.objects.all(), pk=pk)

    if request.method == "GET":
        teacher.delete()
        return HttpResponseRedirect(reverse("teachers:get_teachers"))
