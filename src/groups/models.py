from django.db import models
from faker import Faker


class Group(models.Model):
    name = models.CharField(max_length=120, blank=True, null=True)
    max_students = models.PositiveSmallIntegerField(default=20, null=True)
    start_date = models.DateField(blank=True, null=True)
    count_of_lessons = models.PositiveSmallIntegerField(default=32, null=True)
    speciality = models.CharField(max_length=120, blank=True, null=True)
    address_lessons = models.CharField(max_length=120, blank=True, null=True)

    @classmethod
    def generate_groups(cls, count: int) -> None:
        faker = Faker()
        for i in range(count):
            group = Group(
                name=faker.company(),
                max_students=faker.random_int(min=1, max=20),
                start_date=faker.future_date(end_date="+30d"),
                count_of_lessons=faker.random_int(min=1, max=32),
                speciality=faker.job(),
                address_lessons=faker.address(),
            )
            group.save()

    def __str__(self):
        return f"{self.name} {self.start_date} {self.max_students} {self.speciality} ({self.id})"
