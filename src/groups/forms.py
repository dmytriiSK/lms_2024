from django.forms import ModelForm

from groups.models import Group


class GroupForm(ModelForm):
    class Meta:
        model = Group
        fields = ["name", "max_students", "start_date", "count_of_lessons", "speciality", "address_lessons"]
