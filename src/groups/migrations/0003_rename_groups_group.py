from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("groups", "0002_alter_groups_lessons_alter_groups_max_students"),
    ]

    operations = [
        migrations.RenameModel(
            old_name="Groups",
            new_name="Group",
        ),
    ]
