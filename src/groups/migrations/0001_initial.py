from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Groups",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("group_name", models.CharField(blank=True, max_length=120, null=True)),
                (
                    "max_students",
                    models.PositiveSmallIntegerField(default=0, null=True),
                ),
                ("start_date", models.DateField(blank=True, null=True)),
                ("lessons", models.PositiveSmallIntegerField(default=0, null=True)),
                ("speciality", models.CharField(blank=True, max_length=120, null=True)),
                (
                    "address_lessons",
                    models.CharField(blank=True, max_length=120, null=True),
                ),
            ],
        ),
    ]
