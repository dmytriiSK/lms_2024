from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("groups", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="groups",
            name="lessons",
            field=models.PositiveSmallIntegerField(default=32, null=True),
        ),
        migrations.AlterField(
            model_name="groups",
            name="max_students",
            field=models.PositiveSmallIntegerField(default=20, null=True),
        ),
    ]
