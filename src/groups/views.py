from django.db.models import Q
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from webargs import fields
from webargs.djangoparser import use_args

from groups.forms import GroupForm
from groups.models import Group


@use_args(
    {
        "name": fields.Str(required=False),
        "speciality": fields.Str(required=False),
        "search": fields.Str(required=False),
    },
    location="query",
)
def get_groups(request: HttpRequest, params) -> HttpResponse:
    groups = Group.objects.all()
    search_fields = ["name", "speciality", "start_date"]
    for param_name, param_value in params.items():
        if param_name == "search":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__contains": param_value})
                groups = groups.filter(or_filter)
        else:
            groups = groups.filter(**{param_name: param_value})
    return render(request, template_name="list_group.html", context={"groups": groups})


@csrf_exempt
def create_group(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = GroupForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("groups:get_group"))
    elif request.method == "GET":
        form = GroupForm()
    return render(request, template_name="create_group.html", context={"form": form})


@csrf_exempt
def update_group(request: HttpRequest, pk: int) -> HttpResponse:
    group = get_object_or_404(Group.objects.all(), pk=pk)

    if request.method == "POST":
        form = GroupForm(request.POST, instance=group)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("groups:get_group"))
    elif request.method == "GET":
        form = GroupForm(instance=group)
    return render(request, template_name="update_group.html", context={"form": form})


@csrf_exempt
def delete_group(request: HttpRequest, pk: int) -> HttpResponse:
    group = get_object_or_404(Group.objects.all(), pk=pk)

    if request.method == "GET":
        group.delete()
        return HttpResponseRedirect(reverse("groups:get_group"))
