from django.core.exceptions import ValidationError
from django.forms import ModelForm

from students.models import Student


class StudentForm(ModelForm):
    class Meta:
        model = Student
        fields = ["first_name", "last_name", "email", "birth_date"]

    @staticmethod
    def normalize_text(text):
        return text.strip().capitalize()

    def clean_first_name(self):
        return self.normalize_text(self.cleaned_data["first_name"])

    def clean_last_name(self):
        return self.normalize_text(self.cleaned_data["last_name"])

    def clean_email(self):
        email = self.cleaned_data["email"]

        if "@yandex" in email:
            raise ValidationError("This email not use.")

        return email

    def clean_birth_date(self):
        return self.cleaned_data["birth_date"]
