from django.db.models import Q
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt


from students.forms import StudentForm
from students.models import Student


def index(request: HttpRequest) -> HttpResponse:
    return render(request, template_name="../templates/index.html", context={"name": "LMS"})


def get_students(request):
    search_query = request.GET.get("search", None)

    students = Student.objects.all()
    search_fields = ["first_name", "last_name", "email", "birth_date"]
    if search_query:
        or_filter = Q()
        for field in search_fields:
            or_filter |= Q(**{f"{field}__contains": search_query})
        students = students.filter(or_filter)
    return render(request, template_name="list.html", context={"students": students})


@csrf_exempt
def create_student(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = StudentForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("students:get_students"))
    elif request.method == "GET":
        form = StudentForm()

    return render(request, template_name="create.html", context={"form": form})


@csrf_exempt
def update_student(request: HttpRequest, pk: int) -> HttpResponse:
    student = get_object_or_404(Student.objects.all(), pk=pk)

    if request.method == "POST":
        form = StudentForm(request.POST, instance=student)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("students:get_students"))
    elif request.method == "GET":
        form = StudentForm(instance=student)
    return render(request, template_name="update.html", context={"form": form})


@csrf_exempt
def delete_student(request: HttpRequest, pk: int) -> HttpResponse:
    student = get_object_or_404(Student.objects.all(), pk=pk)

    if request.method == "GET":
        student.delete()
        return HttpResponseRedirect(reverse("students:get_students"))
