import datetime

from django.db import models
from faker import Faker


class Student(models.Model):
    first_name = models.CharField(max_length=120, blank=True, null=True)
    last_name = models.CharField(max_length=120, blank=True, null=True)
    email = models.EmailField(max_length=120)
    grade = models.PositiveSmallIntegerField(default=0, null=True)
    birth_date = models.DateField(blank=True, null=True)

    def age(self):
        return datetime.datetime.now().year - self.birth_date.year

    @classmethod
    def generate_students(cls, count: int) -> None:
        faker = Faker()
        for i in range(count):
            student = Student(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_of_birth(minimum_age=16, maximum_age=60),
            )
            student.save()

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email} ({self.id})"
