from django.conf import settings

from django.contrib import admin
from django.urls import path, include, re_path
from django.views.static import serve

from students.views import index


handler404 = "templates.errors.views.error_404"

urlpatterns = [
    path("admin/", admin.site.urls),
    path("students/", include("students.urls")),
    path("teachers/", include("teachers.urls")),
    path("groups/", include("groups.urls")),
    path("__debug__/", include("debug_toolbar.urls")),
    path("", index, name="index"),
]

urlpatterns += [
    re_path(r"^static/(?P<path>.*)$", serve, {"document_root": settings.STATIC_ROOT}),
    re_path(
        r"^media/(?P<path>.*)$",
        serve,
        {
            "document_root": settings.MEDIA_ROOT,
        },
    ),
]
